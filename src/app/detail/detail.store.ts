import { Injectable } from "@angular/core";
import { ComponentStore, tapResponse } from "@ngrx/component-store";
import { Observable } from "rxjs";
import { mergeMap, tap } from "rxjs/operators";
import { BackendService, Ticket } from "../backend.service";

export interface TicketState {
  ticket?: Ticket;
  loading: boolean;
  error: unknown;
}

const initialState: TicketState = {
  loading: false,
  error: "",
};

@Injectable()
export class DetailStore extends ComponentStore<TicketState> {
  readonly ticket$ = this.select((state) => state.ticket);
  readonly error$ = this.select((state) => state.error);

  constructor(private backend: BackendService) {
    super(initialState);
  }

  readonly loadTicket = this.effect((id$: Observable<number>) => {
    return id$.pipe(
      tap(() => this.patchState({ loading: true, error: "" })),
      mergeMap((id) =>
        this.backend.ticket(id).pipe(
          tapResponse(
            (ticket) =>
              this.patchState({
                loading: false,
                ticket,
              }),
            (error: unknown) => this.patchState({ error })
          )
        )
      )
    );
  });
}
