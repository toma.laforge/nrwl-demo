import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { RouterModule } from "@angular/router";
import { DetailComponent } from "./detail.component";

@NgModule({
  declarations: [DetailComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    RouterModule.forChild([
      {
        path: "",
        component: DetailComponent,
      },
    ]),
  ],
})
export class DetailModule {}
