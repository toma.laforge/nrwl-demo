import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { PARAM_TICKET_ID } from "../app.module";
import { DetailStore } from "./detail.store";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.component.html",
  providers: [DetailStore],
})
export class DetailComponent implements OnInit {
  ticket$ = this.detailStore.ticket$;

  constructor(private detailStore: DetailStore, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.detailStore.loadTicket(this.route.snapshot.params[PARAM_TICKET_ID]);
  }
}
