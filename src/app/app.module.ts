import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { BackendService } from "./backend.service";
import { ListComponent } from "./list/list.component";
import { ListModule } from "./list/list.module";

export const PARAM_TICKET_ID = "ticketId";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    ListModule,
    RouterModule.forRoot([
      {
        path: "",
        pathMatch: "full",
        redirectTo: "list",
      },
      {
        path: "list",
        component: ListComponent,
      },
      {
        path: `detail/:${PARAM_TICKET_ID}`,
        loadChildren: () => import("./detail/detail.module").then((m) => m.DetailModule),
      },
      {
        path: "**",
        redirectTo: "list",
      },
    ]),
  ],
  providers: [BackendService],
  bootstrap: [AppComponent],
})
export class AppModule {}
