import { Component, Input } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { TicketUser } from "src/app/backend.service";
import { TicketStore } from "../../ticket.store";

@Component({
  selector: "app-row",
  templateUrl: "./row.component.html",
  host: {
    class: "p-4 border border-blue-500 rounded flex",
  },
})
export class RowComponent {
  @Input() ticket: TicketUser;

  users$ = this.ticketStore.users$;

  form = new FormGroup({
    assignee: new FormControl(0),
  });

  constructor(private ticketStore: TicketStore) {}

  submit() {
    this.ticketStore.assignTicket({ ticketId: this.ticket.id, userId: this.form.value.assignee });
  }

  done(ticketId: number) {
    this.ticketStore.done(ticketId);
  }
}
