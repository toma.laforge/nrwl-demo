import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { TicketStore } from "../../ticket.store";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
})
export class AddComponent {
  form = new FormGroup({
    description: new FormControl(null, Validators.required),
  });

  loading$ = this.ticketStore.loading$;

  constructor(private ticketStore: TicketStore) {}

  submit() {
    if (this.form.valid) {
      this.ticketStore.addTicket(this.form.value.description);
    }
  }
}
