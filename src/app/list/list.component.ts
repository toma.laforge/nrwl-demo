import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { TicketStore } from "./ticket.store";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"],
  providers: [TicketStore],
})
export class ListComponent implements OnInit {
  tickets$ = this.ticketStore.tickets$;
  loading$ = this.ticketStore.loading$;
  error$ = this.ticketStore.error$;

  search = new FormControl();

  constructor(private ticketStore: TicketStore) {}

  ngOnInit(): void {
    this.ticketStore.loadTickets();
    this.ticketStore.loadUsers();
    this.ticketStore.search(this.search.valueChanges);
  }
}
