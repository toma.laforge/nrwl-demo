import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatSelectModule } from "@angular/material/select";
import { RouterModule } from "@angular/router";
import { ListComponent } from "./list.component";
import { AddComponent } from "./ui/add/add.component";
import { RowComponent } from "./ui/row/row.component";

@NgModule({
  declarations: [ListComponent, AddComponent, RowComponent],
  imports: [
    CommonModule,
    MatProgressBarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [ListComponent],
})
export class ListModule {}
